<?php

define( 'WP_HOME', 'http://portal.local.com' );
define( 'WP_SITEURL', 'http://portal.local.com' );

define( 'DB_NAME', 'local' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', 'root' );
define( 'DB_HOST', 'localhost' );
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

define('AUTH_KEY',         '1VifSeC9K6nrS4Zm2ak6wjKtiuf4HWGiWMaRYAuxDRqYMDVH6+TtmyfKTOz12yrRRnBQggpXCF328QIGeA15qQ==');
define('SECURE_AUTH_KEY',  'mweSPi0Lbc5bDbcMzFgtmLRFYcZCspW0olSqW3DrwPfuZ6HxYXx+itUZqJSuNaZAXRFmczbr2lau1VC2P998hA==');
define('LOGGED_IN_KEY',    'aeFWfx2bZvEZ8eODohjcbrY2uFHcRt6ODywS1iyPYYb7ygdPG5kZHr13XyuM0p9x7E+4xsKjPNs6jaEYsaynpQ==');
define('NONCE_KEY',        'WtRYQSdZb7xz1jo/E/Pwxh/H746qTXMOtmYTpA8NVGf5RmfZP+w+NIutAFO5awPr5LljWovVQ5WlJiqk8nys7w==');
define('AUTH_SALT',        'bA5QD219cb3iQUAMtR67FacqunnynbbCuqN42SWFu2+nohcvwLKZbfypivV/lDFGuGmcoi2h7d2WtSTfJC6Wcg==');
define('SECURE_AUTH_SALT', 'zTBXFZzcqkPUEQ8fUSC5Z1wm0MId1AzEp7SzsIYeCpDpr7eKb4mdW6Bt3/yTxrUMdbaY+kdTEVAogY3pQY8dhQ==');
define('LOGGED_IN_SALT',   'E/gMotPULCa7meRSHjHLdI0laZ79C0JAte+NcyflIcjsFMkgzkD/eSSQYkw32TZ/1i15Tnrr9b3fMK4RGbN2Fg==');
define('NONCE_SALT',       'boCu+Cyz+S4fFuynb+fk9S/3cdH8ywUplxnqIiSJDHkkfs46BB5iZvJHc/CQgwv8DLBJ/Ab/cjaLpoEjbw3mLw==');

$table_prefix = 'wp_';

/* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ) {
	$_SERVER['HTTPS'] = 'on';
}

/* Inserted by Local by Flywheel. Fixes $is_nginx global for rewrites. */
if ( ! empty( $_SERVER['SERVER_SOFTWARE'] ) && strpos( $_SERVER['SERVER_SOFTWARE'], 'Flywheel/' ) !== false ) {
	$_SERVER['SERVER_SOFTWARE'] = 'nginx/1.10.1';
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
