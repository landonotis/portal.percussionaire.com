<?php

///////////////
// ADMIN CSS //
///////////////
function load_admin_style() {
    wp_enqueue_style( 'admin_css', WPMU_PLUGIN_URL . '/admin/admin.css', false, '1.0.0' );
}
add_action( 'admin_enqueue_scripts', 'load_admin_style' );

///////////////
// LOGIN CSS //
///////////////
function load_login_scripts() {
    wp_enqueue_style( 'custom-login', WPMU_PLUGIN_URL . '/admin/login.css' );
    wp_enqueue_script( 'custom-login', WPMU_PLUGIN_URL . '/admin/login.js' );
}
add_action( 'login_enqueue_scripts', 'load_login_scripts' );

//////////////////////////////
// CHANGE LOGIN URL TO HOME //
//////////////////////////////
function login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'login_logo_url' );

//////////////////////
// LOGIN LOGO TITLE //
//////////////////////
function login_logo_title() {
    return 'Percussionaire Portal';
}
add_filter( 'login_headertitle', 'login_logo_title' );