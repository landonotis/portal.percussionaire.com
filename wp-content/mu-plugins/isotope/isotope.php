<?php
/////////////////////////////
// ENQUEUE ISOTOPE SCRIPTS //
/////////////////////////////
function iso_scripts() {
  wp_enqueue_script( 'iso-script', 'https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js', array(), '3.0.6', true );
  wp_enqueue_script( 'iso-init', WPMU_PLUGIN_URL . '/isotope/init.js', null, null, true);
}
add_action( 'wp_enqueue_scripts', 'iso_scripts' );